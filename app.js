const express = require('express');
const app = express();
require('dotenv').config();
const http = require('http').createServer(app);
const io = require('socket.io')(http);
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const chatRouter = require('./routes/chatRouter');
const userRouter = require('./routes/userRouter');
require('./services/passport');

app.use(cors({
    'origin': '*',
    'methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'headers': 'Content-Type, Authorization'
}));

app.use(bodyParser.json());
app.use(passport.initialize());

app.use('/chat', chatRouter);
app.use('/', userRouter);

// Catch unauthorised errors
app.use(function (err, request, response, next) {
    if (err.name === 'UnauthorizedError') {
        response.status(401).json({message: err.name + ': ' + err.message});
    }
});

require('./sockets/sockets')(io);

http.listen(process.env.PORT, function() {
    console.log(`Server is listening ${process.env.PORT} port..`);
});
