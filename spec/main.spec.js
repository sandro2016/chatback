const request = require('request');
// const userService = require("../services/userService");

const baseUrl = 'http://localhost:3000';
const userName = 'test_user';
const userPassword = '111';
let userToken;

describe('main tests', function() {

    describe('wrong attempt to get chat list', function() {
        it('return unauthorized status code', function(done) {
            request.get(baseUrl + '/chat/list', function(error, response, body) {
                expect(response.statusCode).toBe(401);
                done();
            });
        });
    });

    describe('register new user', function() {
        it('create user', function(done) {
            // const tokenPromise = userService.register(userName, userPassword);
            // tokenPromise.then((token) => {
            //     expect(token).not.toEqual(null);
            //     userToken = token;
            //     done();
            // }).catch(error => console.log(error));

            const options = {
                method: 'POST',
                url: baseUrl + '/register',
                json: true,
                body: {
                    name: userName,
                    password: userPassword
                }
            };

            request(options, function(error, response, body) {
                expect(response.statusCode).toEqual(200);
                userToken = body.token;
                done();
            });
        });
    });

    describe('right attempt to get chat list', function() {
        it('return OK status code', function(done) {

            const options = {
                method: 'GET',
                url: baseUrl + '/chat/list',
                headers: {
                    'Authorization': `Bearer ${userToken}`
                }
            };

            request(options, function(error, response, body) {
                expect(response.statusCode).toEqual(200);
                done();
            });
        });
    });
});