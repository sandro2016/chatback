const db = require("../db/models");
const bcrypt = require("bcrypt");
const saltRounds = 10;
const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;

passport.use( new LocalStrategy(
    {
        usernameField: 'name'
    },
    async function(username, password, done) {
        try {
            const user = await db.user.findOne({where: {name: username}});
            if(!user) {
                return done(null, false, {message: 'User not found'});
            }

            const match = await bcrypt.compare(password, user.password);
            if(!match) {
                return done(null, false, {message: 'Password is wrong'});
            }

            return done(null, user);
        } catch (error) {
            return done(error);
        }
    }
));