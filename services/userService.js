require("dotenv").config();
const db = require("../db/models");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;

const register = async (name, password) => {
    try {
        const hash = await new Promise((resolve, reject) => {
            bcrypt.hash(password, saltRounds, function(err, hash) {
                if(err) reject(err);
                resolve(hash);
            });
        });

        const [user, created] = await db.user.findOrCreate({where: {name: name}, defaults: {password: hash}});

        if(!created) {
            throw (new Error('Error during registration'));
        }

        const token = await generateJwt(user);
        return token;
    } catch (error) {
        throw (error);
    }
}

const generateJwt = function(user) {
    return new Promise((resolve, reject) => {
        const expiry = new Date();
        expiry.setDate(expiry.getDate() + 7);

        jwt.sign(
            {
                name: user.name,
                exp: parseInt(expiry.getTime() / 1000)
            },
            process.env.JWT_SECRET,
            function(error, token) {
                if (error) reject(error);
                resolve (token);
            }
        )
    });
}

module.exports = {
    register,
    generateJwt
}