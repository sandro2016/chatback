const db = require("../db/models");

const list = async () => {
    try {
        return await db.chat.findAll({raw: true, include: [{model: db.user}]});
    } catch (error) {
        console.log(error);
        throw error;
    }
}

const create = async (name, owner) => {
    try {
        const user = await db.user.findOne({where: {name: owner}});
        const [chat, created] = await db.chat.findOrCreate({where: {name: name}, defaults: {user_id: user.id}});
        if (created) {
            return chat;
        } else {
            throw new Error("Chat already exists");
        }
    } catch (error) {
        console.log(error);
        throw error;
    }
}

const remove = async (id) => {
    try {
        return await db.chat.destroy({where: {id: id}});
    } catch(error) {
        console.log(error);
        throw error;
    }
}

const get = async (id) => {
    try {
        return await db.chat.findOne({where: {id: id}, raw: true, include: [{model: db.user}]});
    } catch(error) {
        console.log(error);
        throw error;
    }
}

module.exports = {
    list,
    create,
    remove,
    get
}