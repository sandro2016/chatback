require("dotenv").config();
const express = require("express");
const chatController = require("../controllers/chatController");
const jwt = require("express-jwt");

const chatRouter = express.Router();

var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
});

chatRouter.get("/list", auth, chatController.list);
chatRouter.post("/create", auth, chatController.create);
chatRouter.delete("/delete/:id", auth, chatController.remove);
chatRouter.get("/:id", auth, chatController.get);

module.exports = chatRouter;