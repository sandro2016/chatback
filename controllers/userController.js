const userService = require("../services/userService");
const passport = require("passport");

const register = async (request, response) => {
    try {
        const token = await userService.register(request.body.name, request.body.password);

        return response.status(200).json({token: token});
    } catch (error) {
        return response.status(400).json({error: error.message});
    }
}

const login = async (request, response) => {
    try {
        passport.authenticate('local', function(err, user, info) {
            if (err) { return response.status(400).json({error: error.message}); }
            if (!user) { return response.status(400).json({error: info.message}); }
    
            userService.generateJwt(user)
            .then(result => response.status(200).json({token: result}))
            .catch(error => response.status(400).json({error: error.message}));
        })(request, response);
    } catch (error) {
        return response.status(400).json({error: error});
    }
}

module.exports = {
    login,
    register
}