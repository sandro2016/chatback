const chatService = require("../services/chatService");

const list = async (request, response) => {
    try {
        const result = await chatService.list();

        response.status(200).json(result);
    } catch(error) {
        return response.status(400).json({error: error.message});
    }
}

const create = async (request, response) => {
    try {
        const result = await chatService.create(request.body.name, request.body.owner);

        return response.status(201).json(result);
    } catch(error) {
        return response.status(400).json({error: error.message});
    }
}

const remove = async (request, response) => {
    try {   
        const result = await chatService.remove(request.params['id']);

        return response.status(200).json(null);
    } catch(error) {
        return response.status(400).json({error: error.message});
    }
}

const get = async (request, response) => {
    try {
        const result = await chatService.get(request.params['id']);

        return response.status(200).json(result);
    } catch (error) {
        return response.status(400).json({error: error.message});
    }
}

module.exports = {
    list,
    create,
    remove,
    get
}