'use strict';
module.exports = (sequelize, DataTypes) => {
  const chat = sequelize.define('chat', {
    name: DataTypes.STRING,
    user_id: DataTypes.INTEGER,
  }, {});
  chat.associate = function(models) {
    chat.belongsTo(models.user, {
      foreignKey: 'user_id',
      targetKey: 'id'
    });
    chat.hasMany(models.message, {
      foreignKey: 'chat_id',
      sourceKey: 'id',
      as: 'messages'
    });
  };
  return chat;
};