'use strict';
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define('user', {
    name: DataTypes.STRING,
    password: DataTypes.STRING
  }, {});
  user.associate = function (models) {
    user.hasMany(models.chat, {
      foreignKey: 'user_id',
      sourceKey: 'id',
      as: 'chats'
    });
    user.hasMany(models.message, {
      foreignKey: 'user_id',
      sourceKey: 'id',
      as: 'messages'
    });
  };
  return user;
};