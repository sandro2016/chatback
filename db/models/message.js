'use strict';
module.exports = (sequelize, DataTypes) => {
  const message = sequelize.define('message', {
    content: DataTypes.TEXT,
    user_id: DataTypes.INTEGER,
    chat_id: DataTypes.INTEGER
  }, {});
  message.associate = function(models) {
    message.belongsTo(models.user, {
      foreignKey: 'user_id',
      targetKey: 'id'
    });
    message.belongsTo(models.chat, {
      foreignKey: 'chat_id',
      targetKey: 'id'
    });
  };
  return message;
};