module.exports = io => {
    io.on('connection', function(socket){
        console.log('Socket connected');
    
        socket.on('new message', function(data) {
            console.log('message: ' + data);
    
            io.to(socket.room).emit('new message', socket.user + ': ' + data);
        });
    
        socket.on('user joined', function(data) {
            console.log('User has joined the chat');
    
            socket.join(data.room);
    
            socket.room = data.room;
            socket.user = data.user;
    
            io.to(socket.room).emit('user joined', socket.user + ' has joined the chat');
        });
    
        socket.on('disconnect', () => {
            console.log('User has left the chat');
    
            io.to(socket.room).emit('user left', socket.user + ' has left the chat');
        });
    });
}